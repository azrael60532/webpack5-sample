const webpack = require('webpack');
const path = require('path');
const glob = require('glob');
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const mode = process.env.NODE_ENV === 'development' ? 'development' : 'production';
const portfinder = require('portfinder');

const getEntry = () => {
  const entry = {};
  glob.sync('./src/assets/js/entry/*.js').forEach((name) => {
    const lastNode = name.lastIndexOf('/') + 1;
    const enterPath = name.replace('/src', '');
    const n = name.slice(lastNode, name.length - 3); //取得每個js的名稱
    const eArr = [];
    eArr.push(enterPath); //push至陣列中
    entry[n] = eArr; //產生多筆入口的陣列
  });
  return entry;
};

let config = {
  devtool: mode === 'development' ? 'eval-cheap-module-source-map' : 'source-map',
  context: path.resolve(__dirname, 'src'),
  mode: mode,
  stats: {
    children: true,
    errorDetails: true,
  },
  entry: getEntry(),
  output: {
    filename: 'assets/js/[name].min.js?[hash:8]',
    assetModuleFilename: 'assets/img/[name][ext]?[hash:8]',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src/'),
      assets: path.resolve(__dirname, 'src/assets/'),
      sass: path.resolve(__dirname, 'src/assets/sass/'),
      js: path.resolve(__dirname, 'src/assets/js/'),
    },
  },
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        extractComments: false,
      }),
    ],
    splitChunks: {
      cacheGroups: {
        commonjs: {
          test: /[\\/]node_modules[\\/]/, //提取引入的模組
          chunks: 'initial',
          name: 'vendor',
          priority: 2, //檔案的優先順序，數字越大表示優先級越高
          enforce: true,
        },
      },
    },
  },
  devServer: {
    static: 'dist',
    port: 9000,
    compress: true,
    open: true,
    watchFiles: {
      paths: ['src/**/*'],
      options: {
        usePolling: false,
      },
    },
  },
  module: {
    rules: [
      //Pug
      {
        test: /\.pug$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: false },
            // 不壓縮 HTML
          },
          {
            loader: 'pug-html-loader',
            options: {
              pretty: true,
            },
            // 美化 HTML 的編排 (不壓縮HTML的一種)
          },
        ],
      },
      //Sass / Scss
      {
        test: /\.(sa|sc|c)ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true, // 開啟 sourcemap 支持
              url: false,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true, // 開啟 sourcemap 支持
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true, // 開啟 sourcemap 支持
              sassOptions: {
                outputStyle: 'compressed',
              },
            },
          },
        ],
      },
      //Babel
      {
        test: /\.m?js$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-object-rest-spread'],
          },
        },
      },
      //Image
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        type: 'asset',
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      // 指定輸出位置
      // [name] 為上方進入點設定的 "名稱"
      filename: 'assets/css/[name].min.css',
      chunkFilename: 'assets/css/[name].min.css',
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.$': 'jquery',
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: './assets/fonts',
          to: 'assets/fonts/',
        },
        {
          from: './assets/js/static',
          to: 'assets/js/static/',
        },
      ],
    }),
  ],
};

glob.sync('./src/views/**/*.pug').forEach((path) => {
  const start = path.indexOf('/views/') + 7;
  const end = path.length - 4;
  const firstFolder = path.slice(start, end).split('/')[0];
  const name = path.slice(path.lastIndexOf('/') + 1, end);
  const firstLetter = name.split('')[0];
  const outputPath = (firstFolder, name) => {
    if (firstFolder === 'ajax') return `ajax/${name}.html`;
    else return `${name}.html`;
  };
  if (firstLetter === '_') return;
  config.plugins.push(
    new HtmlWebpackPlugin({
      template: path.replace('src/', ''),
      filename: outputPath(firstFolder, name),
      pageName: name,
      inject: firstFolder !== 'ajax' ? 'body' : false,
      scriptLoading: 'blocking',
      chunks: [name],
      minify: {
        sortAttributes: true,
        collapseWhitespace: false,
        collapseBooleanAttributes: true,
        removeComments: false,
      },
    })
  );
});

module.exports = config;
module.exports = new Promise((resolve, reject) => {
  portfinder.basePort = config.devServer.port;
  portfinder.getPort((err, port) => {
    if (err) {
      reject(err);
    } else {
      config.devServer.port = port;
      resolve(config);
    }
  });
});
